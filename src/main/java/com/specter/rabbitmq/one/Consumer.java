package com.specter.rabbitmq.one;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 消费者
 */
public class Consumer {

    /**
     * 队列名称
     */
    public static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setUsername("guest");
        factory.setPassword("guest");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        //声明 消息本身有消息头 消息体
        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println("接受到消息:" + new String(message.getBody()));
        //取消消息时候的回调
        CancelCallback cancelCallback = consumerTag -> {
            System.out.println("消费消息被中断");
        };
        /**
         * 消费消息
         * 1.消费队列名称
         * 2.消费成功之后是否需要自动应答 true 自动应答
         * 3.消费未成功的回调
         * 4.消费者录取消费的回调
         */
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, cancelCallback);
    }
}
